#include <stdio.h>
#include <string.h>

struct Score {
    char name[50];
    float bobot;
    int value;
};

struct Mahasiswa {
    char nim[20];
    char nama[50];
    char matkul[50];
    struct Score scores[5];
};

int checkAvailableMahasiwa (struct Mahasiswa mahasiswa[50], int size, char nim[20]) {
    int available = 1;
    for (int i = 0; i < size; i ++) {
        if (strcmp(mahasiswa[i].nim, nim) == 0) {
            available = 0;
        }
    }

    return available;
}

int main() {

    struct Mahasiswa mahasiswa[50];
    int size = 0;
    int menu;
    char exit = 'n';
    char scoreNames[5][50];
    double scoreBobot[5] = { 0.1, 0.2, 0.1, 0.1, 0.5 };
    int scoreLength = 5;

    // register score names
    strcpy(scoreNames[0], "Nilai Hadir");
    strcpy(scoreNames[1], "Nilai Tugas");
    strcpy(scoreNames[2], "Nilai Quiz");
    strcpy(scoreNames[3], "Nilai Keaktifan Forum");
    strcpy(scoreNames[4], "Nilai UAS");


    do {

        printf("[MENU UTAMA]\n");
        printf("1. Input data mahasiswa \n");
        printf("2. Input nilai mahasiswa \n");
        printf("3. Melihat nilai mahasiswa dan status \n");
        printf("4. Keluar Program \n");
        printf("Pilih Menu : ");
        scanf("%d", &menu);

        if (menu == 1) {
            printf("----------------------- \n");
            printf("[INPUT DATA MAHASISWA] \n");

            // variabel temporary
            char nim[20];
            char nama[50];
            char matkul[50];

            printf("NIM : ");
            scanf("%s", nim);
            printf("Nama : ");
            scanf("%s", nama);
            printf("Matkul : ");
            scanf("%s", matkul);

            // check if mahasiswa is exist
            if (checkAvailableMahasiwa(mahasiswa, size, nim) == 0) {

                printf("----------------------- \n");
                printf("==> Mahasiswa sudah diinputkan sebelumnya ! \n");

            } else {

                strcpy(mahasiswa[size].nim, nim);
                strcpy(mahasiswa[size].nama, nama);
                strcpy(mahasiswa[size].matkul, matkul);

                size += 1;

                printf("----------------------- \n");
                printf("==> Mahasiswa berhasil terdaftar √ \n");
            }

            printf("----------------------- \n");

        }

        else if (menu == 2) {
            char nim[20];

            printf("----------------------- \n");
            printf("[INPUT NILAI MAHASISWA] \n");
            printf("NIM Mahasiswa : ");
            scanf("%s", nim);

            int mhsExist = 0;

            for (int i = 0; i < size; i++) {
                if (strcmp(mahasiswa[i].nim, nim) == 0) {
                    printf("----------------------- \n");
                    printf("Mahasiswa : %s \n", mahasiswa[i].nama);
                    printf("NIM : %s \n", mahasiswa[i].nim);
                    printf("Matkul : %s \n", mahasiswa[i].matkul);
                    printf("----------------------- \n");

                    for (int j = 0; j < scoreLength; j++) {
                        strcpy(mahasiswa[i].scores[j].name, scoreNames[j]);
                        mahasiswa[i].scores[j].bobot = scoreBobot[j];

                        int validate = 0, score;
                        while (!validate) {
                            printf("Nilai %s : ", scoreNames[j]);
                            scanf("%d", &score);

                            if (score <= 100 && score >= 0) {
                                mahasiswa[i].scores[j].value = score;
                                validate = 1;
                            } else {
                                printf("----------------------- \n");
                                printf("==> Range nilai harus (0-100) ! \n");
                                printf("----------------------- \n");
                            }
                        }

                    }

                    printf("----------------------- \n");
                    printf("==> Nilai %s (nim: %s) berhasil diinputkan √ \n", mahasiswa[i].nama, mahasiswa[i].nim);

                    mhsExist = 1;
                }
            }

            if (!mhsExist) {
                printf("----------------------- \n");
                printf("==> Mahasiswa yang diinputkan tidak ada ! \n");

            }

            printf("----------------------- \n");
        } else if (menu == 3) {
            char nim[20];

            printf("----------------------- \n");
            printf("[MELIHAT NILAI MAHASISWA & STATUS] \n");
            printf("NIM Mahasiswa : ");
            scanf("%s", nim);

            struct Mahasiswa mhs;
            int mhsExist = 0;

            for (int i = 0; i < size; i++) {
                if (strcmp(mahasiswa[i].nim, nim) == 0) {
                    mhs = mahasiswa[i];
                    mhsExist = 1;
                }
            }

            if (mhsExist) {

                printf("----------------------- \n");
                printf("Mahasiswa : %s \n", mhs.nama);
                printf("NIM : %s \n", mhs.nim);
                printf("Matkul : %s \n", mhs.matkul);
                printf("----------------------- \n");

                int finalScore = 0;
                char grade;
                char status[20] = "Lulus";

                for (int i = 0; i < scoreLength; i++) {
                    printf("Nilai %s : %d \n", mhs.scores[i].name, mhs.scores[i].value);
                    finalScore += mhs.scores[i].value * mhs.scores[i].bobot;
                }

                if (finalScore >= 90 && finalScore <= 100) {
                    grade = 'A';
                } else if (finalScore >= 80) {
                    grade = 'B';
                } else if (finalScore >= 70) {
                    grade = 'C';
                } else if (finalScore >= 60) {
                    grade = 'D';
                } else if (finalScore >= 50) {
                    grade = 'E';
                } else {
                    grade = 'F';
                }

                if (grade == 'E' || grade == 'F') {
                    strcpy(status, "Tidak Lulus");
                }

                printf("----------------------- \n");
                printf("Final score : %d \n", finalScore);
                printf("----------------------- \n");
                printf("==> Grade dari mahasiswa %s (nim: %s) adalah %c (%s) ! \n", mhs.nama, mhs.nim, grade, status);

            } else {
                printf("----------------------- \n");
                printf("==> Mahasiswa yang diinputkan tidak ada ! \n");
            }

            printf("----------------------- \n");
        } else if (menu == 4){
            exit = 'y';
        } else {
            printf("----------------------- \n");
            printf("Kamu memilih menu yang salah ! \n");
            printf("----------------------- \n");
        }

    } while (exit != 'y');

}


